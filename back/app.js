const express = require("express");
const cors = require("cors");
const app = express();
let corsOptions = {
  origin: "http://localhost:3000"
};
let articles = require('./routes/articles.router');

const db = require("./models/index");
db.mongoose.connect(db.url, {
      useNewUrlParser: true,
      useUnifiedTopology: true
    })
    .then(() => {
      console.log("Connected to the database!");
    })
    .catch(err => {
      console.log("Cannot connect to the database!", err);
      process.exit();
    });

app.use(cors(corsOptions));
// parse requests of content-type - application/json
app.use(express.json());
// parse requests of content-type - application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true }));
// simple route
app.get("/", (req, res) => {
  res.json({ message: "Bienvenue sur Back ENSIBS !" });
});

app.use('/api/articles', articles)

// set port, listen for requests
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`Port :  ${PORT}.`);
});


