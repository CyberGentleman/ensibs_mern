let express = require('express');
let articles = require('../controllers/article.controller');
let router = express.Router();

// Permet de créer un nouvel article de blog
router.post("/", articles.create);

// Récupère l'ensemble des articles du blog
router.get("/", articles.findAll);

// Récupère un article dans le blog
router.get("/:id", articles.findOne);

// Met à jours un article de blog
router.put("/:id", articles.update);

// Supprime un article du blog
router.delete("/:id", articles.delete);

// Supprime tout les élements du blog
router.delete("/", articles.deleteAll);
router.use('/api/articles', router);

module.exports = router;
