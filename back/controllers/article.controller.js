const db = require("../models");
const articles = db.article;

// Permet de créer un nouvel article de blog
exports.create = (req, res) => {
    // Vérifie que l'article est correct
    if (!req.body.title || !req.body.description) {
        res.status(400).send({ message: "Merci d'indiquer un titre et une description !" });
        return;
    }
    // Créer un nouvelle object article
    const article = new articles({
        title: req.body.title,
        description: req.body.description,
        published: req.body.published ? req.body.published : false
    });
    // Sauvegarde l'article dans la BDD
    article
        .save(article)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Une erreur c'est produite dans l'enregistrement dans la base de données."
            });
        });
};

// Récupère l'ensemble des articles du blog
exports.findAll = (req, res) => {
    const title = req.query.title;
    let condition = title ? { title: { $regex: new RegExp(title), $options: "i" } } : {};
    articles.find(condition)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Une erreur c'est produite dans l'enregistrement dans la base de données."
            });
        });
};

// Récupère un article dans le blog
exports.findOne = (req, res) => {
    const id = req.params.id;
    articles.findById(id)
        .then(data => {
            if (!data)
                res.status(404).send({ message: "Pas d'article avec l'id :  " + id });
            else res.send(data);
        })
        .catch(err => {
            res
                .status(500)
                .send({ message: "Une erreur c'est produite dans la recherche de l'article : " + id });
        });
};

// Met à jours un article de blog
exports.update = (req, res) => {
    if (!req.body) {
        return res.status(400).send({
            message: "Les données à mettre à jour ne peuvent pas être vides !"
        });
    }
    const id = req.params.id;
    articles.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
        .then(data => {
            if (!data) {
                res.status(404).send({
                    message: `Impossible de mettre à jour l'article avec id : ${id}. Peut-être que l'article n'a pas été trouvé !`
                });
            } else res.send({ message: "Tutorial was updated successfully." });
        })
        .catch(err => {
            res.status(500).send({
                message: "Une erreur c'est produite dans la mise à jours de l'article : " + id
            });
        });
};

// Supprime un article du blog
exports.delete = (req, res) => {
    const id = req.params.id;
    articles.findByIdAndRemove(id)
        .then(data => {
            if (!data) {
                res.status(404).send({
                    message: `Impossible de supprimer l'article : ${id}.`
                });
            } else {
                res.send({
                    message: "Tutorial was deleted successfully!"
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Une erreur c'est produite dans la suppression de l'article : " + id
            });
        });
};

// Supprime tout les élements du blog
exports.deleteAll = (req, res) => {
    articles.deleteMany({})
        .then(data => {
            res.send({
                message: `${data.deletedCount} articles supprimées avec succès !`
            });
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Une erreur c'est produite dans la base de données."
            });
        });
};
