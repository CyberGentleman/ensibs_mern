### MERN Project ENSIBS Skeleton

Logiciel pour gérer mongo : Mongo Compass

Back = Express + NodeJS
Front = React

Pour start le projet : 

- docker-compose up -d -> Start base de données
- cd ./back && npm run start --> Start API en Express NodeJS
- cd ./front && npm run start --> Start Front React
